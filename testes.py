import unittest
from app import app

class AppTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
    
    def testeRotaTeste(self):
        response = self.app.get('/teste?filme=batman')
        response2 = self.app.get('/teste?filme=filmequenaoexiste')
        resposta = response.json
        resposta2 = response2.json
        self.assertEqual(['Response', 'Search', 'totalResults'], list(resposta.keys()))
        self.assertEqual(['Error', 'Response'], list(resposta2.keys()))


if __name__ == "__main__":
    unittest.main()